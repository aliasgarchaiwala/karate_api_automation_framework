package com.tesla.base;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import com.tesla.utils.ExtentReportHook;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.minidev.json.JSONValue;
public class TestRunner {
    static String karateOutputPath = "target/surefire-reports";
    @BeforeClass
    public static void before() {

    }

    @Test
    public void testParallel() {
		/* Cannot run tests in parallel as some feature file are dependant on others
		and karate runs all feature fils in parallel.
		So below the below parallel no of threads is set to 1.
		*/
        // List<String> tags = Arrays.asList(System.getProperty("tags").split(","));
        String tags = System.getProperty("tags");
        String[] paths = "classpath:com/tesla".split(",");
        Results stats1 = Runner.path(paths).tags(tags, "@regressiondb").reportDir(karateOutputPath).hook(new ExtentReportHook()).parallel(1);

        assertTrue("there are scenario failures", (stats1.getFailCount()) == 0);
    }

    @AfterClass
    public static void after(){
        // commented cucumer html reports as we are generating extent html report
        generateReport(karateOutputPath);
    }

    private static void generateReport(String karateOutputPath) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date = new Date();
        String currentDate = dateFormat.format(date);

        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] { "json" }, true);
        List<String> jsonPaths = new ArrayList<String>(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));

        // To Store reports in other location in system
        //Configuration config = new Configuration(new File("C:/Users/Toshiba/Documents/KarateResults/" + currentDate), "tesla Functional Test");
        Configuration config = new Configuration(new File("target/" + currentDate), "tesla Test Automation Results");

        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }

    public static String converToString(String conValue) {
        return conValue.replaceAll("\"", "");
    }

    public static String getYamlProperties(String yamlString) {
        Yaml yaml= new Yaml();
        Object obj = yaml.load(yamlString);

        return JSONValue.toJSONString(obj);
    }
}
