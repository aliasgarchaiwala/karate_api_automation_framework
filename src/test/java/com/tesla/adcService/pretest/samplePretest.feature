Feature: Sample Pretest1

  Background:

    * def jsUtils = read('classpath:com/tesla/utils/jsUtils.js')
  	# calling apportion Json
    * def samplePostRequest = read('../../adcService/requestPayload/sample/samplePost.json')
    * configure headers = read('classpath:com/tesla/utils/websCommonHeaders.js')


  @success
  Scenario: Sample Get

    Given url sampleUrlget
    When method get
    Then status 200
    And def sampleResponseHeader = responseHeaders
    And def sampleResponseBody = response


  @successpost
  Scenario: Sample Post

    Given url sampleUrlPost
    And request samplePostRequest
    When method post
    Then status 201
    And def samplePostResponseHeader = responseHeaders
    And def samplePostResponseBody = response
