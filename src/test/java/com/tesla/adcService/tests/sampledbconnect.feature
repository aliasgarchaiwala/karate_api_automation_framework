Feature: Connection to database

  Background:
    * def commonConstants = read('../../common-services/constants/genericConstants.yaml')
    * def dbUtils = Java.type('com.tesla.utils.dbUtils')
    * def db = new dbUtils(commonConstants.dbconfig.cockroachdb_defaultdb)

  @regressiondb
  Scenario:
    * def users = db.readRows('select * from movr."users"')
    Then print 'users--', users[0].id