Feature: Sample Feature1


  Background:
  * def jsUtils = read('classpath:com/tesla/utils/jsUtils.js')
  * configure headers = read('classpath:com/tesla/utils/websCommonHeaders.js')
  * def sampleServiceData = read('../../adcService/constants/sampleService.yaml')
  * def commonConstants = read('../../common-services/constants/genericConstants.yaml')


  @regression
  Scenario: Test GET Call
  * call read('../../adcService/pretest/samplePretest.feature@success')
  * assert sampleResponseBody.length != 0

  @regression
  Scenario: Test POST Call
  * call read('../../adcService/pretest/samplePretest.feature@successpost')
  * assert samplePostResponseBody.length != 0
  * match samplePostResponseBody.name == '#present'
