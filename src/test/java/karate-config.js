function fn() {
    var env = karate.env; // get system property 'karate.env'
    var locale = karate.properties['locale']; // get system property 'karate.locale'
    var tenantId = karate.properties['tenantId']; // get system property 'karate.tenantId'

    if(!locale){
    	locale = 'en_IN';
    }

    if(!karate.properties['configPath']){
        karate.log(java.lang.String.format("Terminating execution due to %s config file path",
        karate.properties['configPath']))
        java.lang.System.exit(0);
    }

    var envProps = karate.read('file:' + karate.properties['configPath']);

    var path = karate.read('file:envYaml/common/common.yaml');

  try{

   var config = {
         env : env,
         basicAuthorization: envProps.basicAuthorization,
         locale : locale,
         retryCount : 30,
         retryInterval : 10000 //ms
   };

   if(karate.properties['useKafkaSimulation']){
       if(karate.properties['useKafkaSimulation'] == 'yes'){
        config.kafkaHost = envProps.mockHost
       }
   }else{
    config.kafkaHost = envProps.host
   }

        config.envHost = envProps.host
        config.envLocalhost = envProps.localhost
        config.envMockHost = envProps.mockhost

        //authTokenUrl
        //config.authTokenUrl = envProps.host + path.endPoints.authenticationToken.authToken;
        //sample
         config.sampleUrlget = envProps.host + path.endPoints.sample.get;
         config.sampleUrlPost = envProps.host + path.endPoints.sample.post;




        var driverConfig = { type: 'chrome', headless: false, addOptions: [ '--disable-geolocation', '--start-maximized', '--disable-notifications'], prefs : { 'profile.default_content_setting_values.geolocation': 2} };
        karate.configure('driver', driverConfig);
        config.driverConfig = driverConfig;

    karate.log('karate.env:', env);
    karate.log('locale:', locale);

    karate.configure('readTimeout', 120000);

    return config;
    }catch(e){
        karate.log(java.lang.String.format("Terminating execution due to %s in configuration", e))
        java.lang.System.exit(0);
    }
}